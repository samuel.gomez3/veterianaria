package co.edu.fumc.Veterinaria.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import co.edu.fumc.Veterinaria.entities.Appointment;

public interface AppointmentRepository extends JpaRepository<Appointment, Integer> {

}
