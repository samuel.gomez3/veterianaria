package co.edu.fumc.Veterinaria.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import co.edu.fumc.Veterinaria.entities.Cie10;

public interface Cie10Repository extends JpaRepository<Cie10, Integer> {

	Optional<Cie10> findByCodeCie10(String codeCie10);

}
