package co.edu.fumc.Veterinaria.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import co.edu.fumc.Veterinaria.entities.Owner;

public interface OwnerRepository extends JpaRepository<Owner, Integer> {

}
