package co.edu.fumc.Veterinaria.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import co.edu.fumc.Veterinaria.entities.TypeAppointment;

public interface TypeAppointmentRepository extends JpaRepository<TypeAppointment, Integer> {

}
