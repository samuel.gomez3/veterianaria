package co.edu.fumc.Veterinaria.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import co.edu.fumc.Veterinaria.entities.Pet;

public interface PetRepository extends JpaRepository<Pet, Integer> {

	Optional<Pet> findByName(String name);

}
