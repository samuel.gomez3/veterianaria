package co.edu.fumc.Veterinaria.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import co.edu.fumc.Veterinaria.entities.Bill;

public interface BillRepository extends JpaRepository<Bill, Integer> {

}
