package co.edu.fumc.Veterinaria.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import co.edu.fumc.Veterinaria.dtos.BillDtoRq;
import co.edu.fumc.Veterinaria.entities.Bill;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface BillMapper {
	Bill toBill(BillDtoRq billRq);
}
