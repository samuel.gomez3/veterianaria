package co.edu.fumc.Veterinaria.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import co.edu.fumc.Veterinaria.dtos.DiagnosisDtoRq;
import co.edu.fumc.Veterinaria.entities.Diagnosis;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface DiagnosisMapper {
	Diagnosis toDiagnosis(DiagnosisDtoRq diagnosisRq);
}
