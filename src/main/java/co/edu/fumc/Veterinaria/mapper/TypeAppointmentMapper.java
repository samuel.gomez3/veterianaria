package co.edu.fumc.Veterinaria.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import co.edu.fumc.Veterinaria.dtos.TypeAppointmentDtoRq;
import co.edu.fumc.Veterinaria.entities.TypeAppointment;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface TypeAppointmentMapper {

	TypeAppointment toTypeAppointment(TypeAppointmentDtoRq appointmentDtoRq);

}
