package co.edu.fumc.Veterinaria.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import co.edu.fumc.Veterinaria.dtos.PetDtoRq;
import co.edu.fumc.Veterinaria.entities.Pet;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface PetMapper {

	Pet toPet(PetDtoRq dtoRq);

}
