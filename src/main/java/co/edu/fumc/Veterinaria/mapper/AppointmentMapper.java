package co.edu.fumc.Veterinaria.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import co.edu.fumc.Veterinaria.dtos.AppointmentDtoRq;
import co.edu.fumc.Veterinaria.entities.Appointment;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface AppointmentMapper {
	Appointment toAppointment(AppointmentDtoRq appointmentRq);
}
