package co.edu.fumc.Veterinaria.dtos;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppointmentDtoRq {
	private LocalDateTime dateAppointment;
	private int idPet;
	private int idTypeAppointment;
}
