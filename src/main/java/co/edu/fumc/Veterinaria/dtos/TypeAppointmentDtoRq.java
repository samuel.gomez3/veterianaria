package co.edu.fumc.Veterinaria.dtos;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TypeAppointmentDtoRq {

	private String description;
	private BigDecimal value;
	private int durationTimeInMinutes;

}
