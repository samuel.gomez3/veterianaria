package co.edu.fumc.Veterinaria.dtos;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PetDtoRq {

	private String name;
	private String breedPet;
	private BigDecimal weight;
	private String detail;
	private int idTutorMain;
	private int idTutorSecondary;

}
