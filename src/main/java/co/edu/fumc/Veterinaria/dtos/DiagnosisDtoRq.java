package co.edu.fumc.Veterinaria.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DiagnosisDtoRq {
	private int idAppointment;
	private int idcie10Main;
	private int idcie10Secondary;
	private String petSymptoms;
	private String treatment;
}
