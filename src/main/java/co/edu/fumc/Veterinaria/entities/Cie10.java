package co.edu.fumc.Veterinaria.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "cie10")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cie10 {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_cie10")
	private int idCie10;

	@Column(name = "code_cie10", nullable = false, length = 10)
	private String codeCie10;

	@Column(name = "description", nullable = false, columnDefinition = "TEXT")
	private String description;

}
