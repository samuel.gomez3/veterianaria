package co.edu.fumc.Veterinaria.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "owner")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Owner {

    @Id
    @Column(name = "id_owner")
    private int idOwner;

    @Column(name = "type_id", nullable = false, length = 20)
    private String typeId;

    @Column(name = "name", nullable = false, length = 50)
    private String name;

    @Column(name = "lastname", nullable = false, length = 50)
    private String lastname;

    @Column(name = "fullname", nullable = false, length = 100)
    private String fullname;

    @Column(name = "address", nullable = false, length = 100)
    private String address;

    @Column(name = "neighborhood", nullable = false, length = 50)
    private String neighborhood;

    @Column(name = "email", nullable = false, length = 100)
    private String email;

    @Column(name = "phone", nullable = false, length = 20)
    private String phone;

    @Column(name = "name_group_family", nullable = false, length = 50)
    private String nameGroupFamily;

}
