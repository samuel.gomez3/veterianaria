package co.edu.fumc.Veterinaria.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "diagnosis")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Diagnosis {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_diagnosis")
    private int idDiagnosis;

    @Column(name = "pet_symptoms", nullable = false, columnDefinition = "TEXT")
    private String petSymptoms;

    @ManyToOne
    @JoinColumn(name = "cie10_main", nullable = false)
    private Cie10 cie10Main;

    @ManyToOne
    @JoinColumn(name = "cie10_secondary", nullable = false)
    private Cie10 cie10Secondary;

    @Column(name = "treatment", nullable = false, columnDefinition = "TEXT")
    private String treatment;

    @ManyToOne
    @JoinColumn(name = "id_appointment", nullable = false)
    private Appointment appointment;

}
