package co.edu.fumc.Veterinaria.entities;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "type_appointment")
@AllArgsConstructor
@Data
public class TypeAppointment {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_type_appointment")
	private int idTypeAppointment;

	@Column(name = "description", nullable = false, length = 100)
	private String description;

	@Column(name = "value", nullable = true, precision = 8, scale = 2)
	private BigDecimal value;

	@Column(name = "status", nullable = false)
	private boolean status;

	@Column(name = "duration_time_in_minutes", nullable = false)
	private int durationTimeInMinutes;
	
	public TypeAppointment() {
	     this.status = true;
	 }
}
