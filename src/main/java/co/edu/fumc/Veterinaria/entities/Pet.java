package co.edu.fumc.Veterinaria.entities;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "pet")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Pet {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_pet")
    private int idPet;

    @Column(name = "name", nullable = false, length = 50)
    private String name;

    @Column(name = "breed_pet", nullable = false, length = 50)
    private String breedPet;

    @Column(name = "weight", nullable = false, precision = 5, scale = 2)
    private BigDecimal weight;

    @Column(name = "detail", nullable = false)
    private String detail;

    @ManyToOne
    @JoinColumn(name = "id_tutor_main", nullable = false)
    private Owner tutorMain;

    @ManyToOne
    @JoinColumn(name = "id_tutor_secondary", nullable = true)
    private Owner tutorSecondary;

}
