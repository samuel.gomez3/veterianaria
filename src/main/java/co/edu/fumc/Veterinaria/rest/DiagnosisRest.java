package co.edu.fumc.Veterinaria.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.edu.fumc.Veterinaria.dtos.AppointmentDtoRq;
import co.edu.fumc.Veterinaria.dtos.DiagnosisDtoRq;
import co.edu.fumc.Veterinaria.entities.Appointment;
import co.edu.fumc.Veterinaria.entities.Cie10;
import co.edu.fumc.Veterinaria.entities.Diagnosis;
import co.edu.fumc.Veterinaria.mapper.DiagnosisMapper;
import co.edu.fumc.Veterinaria.repositories.AppointmentRepository;
import co.edu.fumc.Veterinaria.repositories.Cie10Repository;
import co.edu.fumc.Veterinaria.repositories.DiagnosisRepository;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/Diagnosis")
@RequiredArgsConstructor
public class DiagnosisRest {
	final DiagnosisRepository diagnosisRepository;
	final AppointmentRepository appointmentRepository;
	final Cie10Repository cie10Repository;
	final DiagnosisMapper diagnosisMapper;
	
	@GetMapping()
	public ResponseEntity<Object> getDiagnosis(){
		return new ResponseEntity<>(diagnosisRepository.findAll(), HttpStatus.OK);
	}
	@GetMapping("/{id}")
	public ResponseEntity<Object> getDiagnosisById(@PathVariable("id") int id){
		return new ResponseEntity<>(diagnosisRepository.findById(id).orElse(null), HttpStatus.OK);
	}
	@PostMapping()
	public ResponseEntity<Object> createDiagnosis(@RequestBody DiagnosisDtoRq diagnosisDtoRq){
		Appointment appointment = appointmentRepository.findById(diagnosisDtoRq.getIdAppointment()).orElse(null);
		Cie10 cie10Main = cie10Repository.findById(diagnosisDtoRq.getIdcie10Main()).orElse(null);
		Cie10 cie10Secondary = cie10Repository.findById(diagnosisDtoRq.getIdcie10Secondary()).orElse(null);
		Diagnosis diagnosis = diagnosisMapper.toDiagnosis(diagnosisDtoRq);
		diagnosis.setAppointment(appointment);
		diagnosis.setCie10Main(cie10Main);
		diagnosis.setCie10Secondary(cie10Secondary);
		return new ResponseEntity<>(diagnosisRepository.save(diagnosis), HttpStatus.OK);
		
	}
	@PutMapping("/{id}")
	public ResponseEntity<Object> updateDiagnosis(@PathVariable("id") int id, @RequestBody DiagnosisDtoRq diagnosisDtoRq){
		Appointment appointment = appointmentRepository.findById(diagnosisDtoRq.getIdAppointment()).orElse(null);
		Cie10 cie10Main = cie10Repository.findById(diagnosisDtoRq.getIdcie10Main()).orElse(null);
		Cie10 cie10Secondary = cie10Repository.findById(diagnosisDtoRq.getIdcie10Secondary()).orElse(null);
		Diagnosis diagnosis = diagnosisMapper.toDiagnosis(diagnosisDtoRq);
		diagnosis.setIdDiagnosis(id);
		diagnosis.setAppointment(appointment);
		diagnosis.setCie10Main(cie10Main);
		diagnosis.setCie10Secondary(cie10Secondary);
		return new ResponseEntity<>(diagnosisRepository.save(diagnosis), HttpStatus.OK);
		
	}
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> deleteDiagnosis(@PathVariable("id") int id) {
		diagnosisRepository.deleteById(id);
		return new ResponseEntity<>(null, HttpStatus.OK);
	}
	
}
