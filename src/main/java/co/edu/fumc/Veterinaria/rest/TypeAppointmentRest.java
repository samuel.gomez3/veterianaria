package co.edu.fumc.Veterinaria.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.edu.fumc.Veterinaria.dtos.TypeAppointmentDtoRq;
import co.edu.fumc.Veterinaria.entities.TypeAppointment;
import co.edu.fumc.Veterinaria.mapper.TypeAppointmentMapper;
import co.edu.fumc.Veterinaria.repositories.TypeAppointmentRepository;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/TypeAppointment")
@RequiredArgsConstructor
public class TypeAppointmentRest {

	final TypeAppointmentRepository appointmentRepository;
	final TypeAppointmentMapper typeAppointmentMapper;

	@GetMapping()
	public ResponseEntity<Object> getTypeAppointment() {
		return new ResponseEntity<>(appointmentRepository.findAll(), HttpStatus.OK);
	}

	@PostMapping()
	public ResponseEntity<Object> createTypeAppointment(@RequestBody TypeAppointmentDtoRq typeAppointment) {
		return new ResponseEntity<>(
				appointmentRepository.save(typeAppointmentMapper.toTypeAppointment(typeAppointment)), HttpStatus.OK);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Object> updateTypeAppointment(@PathVariable("id") int id,
			@RequestBody TypeAppointmentDtoRq typeAppointmentDto) {
		TypeAppointment typeAppointmentModel = typeAppointmentMapper.toTypeAppointment(typeAppointmentDto);
		typeAppointmentModel.setIdTypeAppointment(id);
		return new ResponseEntity<>(appointmentRepository.save(typeAppointmentModel), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Object> getTypeAppointmentById(@PathVariable("id") int id) {
		return new ResponseEntity<>(appointmentRepository.findById(id).orElse(null), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Object> deleteTypeAppointment(@PathVariable("id") int id) {
		appointmentRepository.deleteById(id);
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

}
