package co.edu.fumc.Veterinaria.rest;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.edu.fumc.Veterinaria.dtos.AppointmentDtoRq;
import co.edu.fumc.Veterinaria.dtos.BillDtoRq;
import co.edu.fumc.Veterinaria.dtos.DiagnosisDtoRq;
import co.edu.fumc.Veterinaria.entities.Appointment;
import co.edu.fumc.Veterinaria.entities.Bill;
import co.edu.fumc.Veterinaria.entities.Cie10;
import co.edu.fumc.Veterinaria.entities.Diagnosis;
import co.edu.fumc.Veterinaria.entities.TypeAppointment;
import co.edu.fumc.Veterinaria.mapper.BillMapper;
import co.edu.fumc.Veterinaria.mapper.DiagnosisMapper;
import co.edu.fumc.Veterinaria.repositories.AppointmentRepository;
import co.edu.fumc.Veterinaria.repositories.BillRepository;
import co.edu.fumc.Veterinaria.repositories.Cie10Repository;
import co.edu.fumc.Veterinaria.repositories.DiagnosisRepository;
import co.edu.fumc.Veterinaria.repositories.TypeAppointmentRepository;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/Bill")
@RequiredArgsConstructor
public class BillRest {
	final BillRepository billRepository;
	final AppointmentRepository appointmentRepository;
	final BillMapper billMapper;
	final TypeAppointmentRepository typeAppointmentRepository;
	AppointmentDtoRq appointmentDtoRq;
	
	@GetMapping()
	public ResponseEntity<Object> getBill(){
		return new ResponseEntity<>(billRepository.findAll(), HttpStatus.OK);
	}
	@GetMapping("/{id}")
	public ResponseEntity<Object> getBillById(@PathVariable("id") int id){
		return new ResponseEntity<>(billRepository.findById(id).orElse(null), HttpStatus.OK);
	}
	@PutMapping("/{id}/{status}")
	public ResponseEntity<Object> updateStatusBill(@PathVariable("id") int id, @PathVariable("status") String status){
		Bill bill = billRepository.findById(id).orElse(null);
		bill.setIdBill(id);
		bill.setStatus(status);
		return new ResponseEntity<>(billRepository.save(bill),HttpStatus.OK);
	}
}
