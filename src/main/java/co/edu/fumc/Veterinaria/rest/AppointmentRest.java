package co.edu.fumc.Veterinaria.rest;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import co.edu.fumc.Veterinaria.dtos.AppointmentDtoRq;
import co.edu.fumc.Veterinaria.entities.Appointment;
import co.edu.fumc.Veterinaria.entities.Bill;
import co.edu.fumc.Veterinaria.entities.Pet;
import co.edu.fumc.Veterinaria.entities.TypeAppointment;
import co.edu.fumc.Veterinaria.mapper.AppointmentMapper;
import co.edu.fumc.Veterinaria.mapper.BillMapper;
import co.edu.fumc.Veterinaria.repositories.AppointmentRepository;
import co.edu.fumc.Veterinaria.repositories.BillRepository;
import co.edu.fumc.Veterinaria.repositories.PetRepository;
import co.edu.fumc.Veterinaria.repositories.TypeAppointmentRepository;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/Appointment")
@RequiredArgsConstructor
public class AppointmentRest {
	final AppointmentRepository appointmentRepository;
	final TypeAppointmentRepository typeAppointmentRepository;
	final PetRepository petRepository;
	final AppointmentMapper appointmentMapper;
	final BillMapper billMapper;
	final BillRepository billRepository;

	@GetMapping()
	public ResponseEntity<Object> getAppointment() {
		return new ResponseEntity<>(appointmentRepository.findAll(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Object> getAppointmentById(@PathVariable("id") int id) {
		return new ResponseEntity<>(appointmentRepository.findById(id).orElse(null), HttpStatus.OK);
	}

	@PostMapping()
	public ResponseEntity<Object> createAppointment(@RequestBody AppointmentDtoRq appointmentDtoRq) {
		TypeAppointment typeAppointment = typeAppointmentRepository.findById(appointmentDtoRq.getIdTypeAppointment())
				.orElse(null);
		Pet pet = petRepository.findById(appointmentDtoRq.getIdPet()).orElse(null);
		Appointment appointment = appointmentMapper.toAppointment(appointmentDtoRq);
		appointment.setTypeAppointment(typeAppointment);
		appointment.setPet(pet);
		return new ResponseEntity<>(appointmentRepository.save(appointment), HttpStatus.OK);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Object> updateAppointment(@PathVariable("id") int id,
			@RequestBody AppointmentDtoRq appointmentDtoRq) {
		TypeAppointment typeAppointment = typeAppointmentRepository
				.findById(appointmentDtoRq.getIdTypeAppointment()).orElse(null);
	
		Pet pet = petRepository.findById(appointmentDtoRq.getIdPet()).orElse(null);
		Appointment appointment = appointmentMapper.toAppointment(appointmentDtoRq);
		appointment.setIdAppointment(id);
		appointment.setTypeAppointment(typeAppointment);
		appointment.setPet(pet);
		return new ResponseEntity<>(appointmentRepository.save(appointment), HttpStatus.OK);
	}

	@PutMapping("/{id}/{status}")
	public ResponseEntity<Object> updateStatusAppointment(@PathVariable("id") int id,
			@PathVariable("status") boolean status) {
		Appointment appointment = appointmentRepository.findById(id).orElse(null);
		appointment.setIdAppointment(id);
		appointment.setStatus(status);
		appointmentRepository.save(appointment);
		if (!status) {
			TypeAppointment typeAppointment = appointment.getTypeAppointment();
			Bill bill = new Bill();
			bill.setAppointment(appointment);
			bill.setBillingDate(LocalDate.now());
			bill.setTotalValue(typeAppointment.getValue().multiply(new BigDecimal("1.19")));
			billRepository.save(bill);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Object> deleteAppointment(@PathVariable("id") int id) {
		appointmentRepository.deleteById(id);
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

}
