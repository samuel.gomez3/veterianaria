package co.edu.fumc.Veterinaria.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.edu.fumc.Veterinaria.entities.Owner;
import co.edu.fumc.Veterinaria.repositories.OwnerRepository;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/Owner")
@RequiredArgsConstructor
public class OwnerRest {

	final OwnerRepository ownerRepository;

	@GetMapping()
	public ResponseEntity<Object> getOwner() {
		return new ResponseEntity<>(ownerRepository.findAll(), HttpStatus.OK);
	}

	@PostMapping()
	public ResponseEntity<Object> createOwner(@RequestBody Owner owner) {
		return new ResponseEntity<>(ownerRepository.save(owner), HttpStatus.OK);
	}

	@PutMapping()
	public ResponseEntity<Object> updateOwner(@RequestBody Owner owner) {
		return new ResponseEntity<>(ownerRepository.save(owner), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Object> getOwnerById(@PathVariable("id") int id) {
		return new ResponseEntity<>(ownerRepository.findById(id).orElse(null), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Object> deleteOwner(@PathVariable("id") int id) {
		ownerRepository.deleteById(id);
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

}
