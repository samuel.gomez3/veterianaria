package co.edu.fumc.Veterinaria.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.edu.fumc.Veterinaria.dtos.PetDtoRq;
import co.edu.fumc.Veterinaria.dtos.TypeAppointmentDtoRq;
import co.edu.fumc.Veterinaria.entities.Owner;
import co.edu.fumc.Veterinaria.entities.Pet;
import co.edu.fumc.Veterinaria.entities.TypeAppointment;
import co.edu.fumc.Veterinaria.mapper.PetMapper;
import co.edu.fumc.Veterinaria.repositories.OwnerRepository;
import co.edu.fumc.Veterinaria.repositories.PetRepository;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/Pet")
@RequiredArgsConstructor
public class PetRest {

	final PetRepository petRepository;
	final OwnerRepository ownerRepository;
	final PetMapper petMapper;

	@GetMapping()
	public ResponseEntity<Object> getPet() {
		return new ResponseEntity<>(petRepository.findAll(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Object> getPetById(@PathVariable("id") int id) {
		return new ResponseEntity<>(petRepository.findById(id).orElse(null), HttpStatus.OK);
	}

	@GetMapping("/name/{name}")
	public ResponseEntity<Object> getPetByName(@PathVariable("name") String name) {
		return new ResponseEntity<>(petRepository.findByName(name).orElse(null), HttpStatus.OK);
	}

	@PostMapping()
	public ResponseEntity<Object> createPet(@RequestBody PetDtoRq petDtoRq) {
		Owner ownerMain = ownerRepository.findById(petDtoRq.getIdTutorMain()).orElse(null);
		Owner ownerSecundary = ownerRepository.findById(petDtoRq.getIdTutorSecondary()).orElse(null);
		Pet pet = petMapper.toPet(petDtoRq);
		pet.setTutorMain(ownerMain);
		pet.setTutorSecondary(ownerSecundary);
		return new ResponseEntity<>(petRepository.save(pet), HttpStatus.OK);
	}
	@PutMapping("/{id}")
	public ResponseEntity<Object> updatePet(@PathVariable("id") int id, @RequestBody PetDtoRq petDtoRq) {
		Owner ownerMain = ownerRepository.findById(petDtoRq.getIdTutorMain()).orElse(null);
		Owner ownerSecundary = ownerRepository.findById(petDtoRq.getIdTutorSecondary()).orElse(null);
		Pet petModel = petMapper.toPet(petDtoRq);
		petModel.setIdPet(id);
		petModel.setTutorMain(ownerMain);
		petModel.setTutorSecondary(ownerSecundary);
		return new ResponseEntity<>(petRepository.save(petModel), HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> deletePet(@PathVariable("id") int id) {
		petRepository.deleteById(id);
		return new ResponseEntity<>(null, HttpStatus.OK);
	}
}
