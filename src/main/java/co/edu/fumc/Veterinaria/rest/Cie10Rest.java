package co.edu.fumc.Veterinaria.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.edu.fumc.Veterinaria.repositories.Cie10Repository;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/Cie10")
@RequiredArgsConstructor
public class Cie10Rest {
	
	final Cie10Repository cie10Repository;
	
	@GetMapping()
	public ResponseEntity<Object> getCie10() {
		return new ResponseEntity<>(cie10Repository.findAll(), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Object> getCie10ById(@PathVariable("id") int id) {
		return new ResponseEntity<>(cie10Repository.findById(id).orElse(null), HttpStatus.OK);
	}
	
	@GetMapping("/code/{code}")
	public ResponseEntity<Object> getCie10ByCode(@PathVariable("code") String code) {
		return new ResponseEntity<>(cie10Repository.findByCodeCie10(code).orElse(null), HttpStatus.OK);
	}

}
